//进行远程调用
import axios from 'axios';
import qs from 'qs';
//let base = '';

// export const requestLogin = params => { return axios.post(`${base}/login`, params).then(res => res.data); };

//模块
export const getLineList = params =>{return axios.get(`/api/data/ViewData?page=1`,params)};
export const getTableList = params =>{return axios.get(`/api/top/goods`,params)};
export const getTable2List = params =>{return axios.get(`/api/top/cate`,params)};
export const getColumnWeekList = params =>{return axios.get(`/api/sale/week`)};
export const getColumnMonthList = params =>{return axios.get(`/api/sale/month`)};
export const getColumnSeasonList = params =>{return axios.get(`/api/sale/season`)};
export const getSale = params =>{return axios.get(`/api/sale/all`)};
export const modifyData = params =>{return axios.get(`/api/control/set/`.concat(params.value))};
export const addData = params =>{return axios.get(`/api/control/add/`.concat(params.value))};
export const downData = params =>{return axios.get(`/api/control/down/`.concat(params.value))};
export const insertData = params =>{return axios.get(`/api/control/insert/`.concat(params.value))};




